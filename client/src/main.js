import BootstrapVue from 'bootstrap-vue';
import Vue from 'vue';
import LoadScript from 'vue-plugin-load-script';
import App from './App.vue';
import router from './router';
import 'bootstrap/dist/css/bootstrap.css';
import { domain, clientId } from '../auth_config.json';
import { Auth0Plugin } from './auth';

Vue.use(BootstrapVue);
Vue.use(LoadScript);
Vue.use(Auth0Plugin, {
  domain,
  clientId,
  onRedirectCallback: (appState) => {
    router.push(
      appState && appState.targetUrl
        ? appState.targetUrl
        : window.location.pathname,
    );
  },
});

Vue.config.productionTip = false;

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app');
