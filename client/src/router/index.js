import Vue from 'vue';
import Router from 'vue-router';
import Books from '../components/Books.vue';
import Main from '../components/Main.vue';
import Profile from '../components/Profile.vue';
import authGuard from '../auth/authGuard';

Vue.use(Router);

const DEFAULT_TITLE = 'NDI-TAP WebApp';

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Main,
      meta: { title: 'NDI-TAP WebApp' },
    },
    {
      path: '/books',
      name: 'Books',
      component: Books,
      meta: { title: 'Books Skeleton' },
    },
    {
      path: '/profile',
      name: 'profile',
      component: Profile,
      beforeEnter: authGuard,
    },
  ],
});

router.afterEach((to) => {
  Vue.nextTick(() => {
    document.title = to.meta.title || DEFAULT_TITLE;
  });
});

export default router;
